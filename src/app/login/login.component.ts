import { Component, OnInit, ElementRef } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Router } from '@angular/router';
// import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginService } from '../_services/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [
   trigger('cardFlip', [
      state('default', style({
        transform: 'none'
      })),
      state('flipped', style({
        transform: 'rotateY(180deg)'
      })),
      transition('default => flipped', [
        animate('400ms')
      ]),
      transition('flipped => default', [
        animate('200ms')
      ])
    ])
  ]
})
export class LoginComponent implements OnInit {

  userName :any;
  password : any;
  constructor(private el: ElementRef,
    private router : Router,
    private loginService : LoginService,
    private _snackBar : MatSnackBar) { }

  

  ngOnInit(): void {
  }

  data: CardData = {
    imageId: "pDGNBK9A0sk",
    state: "default"
  };

  cardClicked() {
    if (this.data.state === "default") {
      this.data.state = "flipped";
    } else {
      this.data.state = "default";
    }
  }

  signUp(){
    // alert("signup")
    this.el.nativeElement.querySelector("#sign-up-btn").addEventListener("click", () => {
      this.el.nativeElement.querySelector(".container").classList.add("sign-up-mode");
    });
  }

  signIn(){
    this.el.nativeElement.querySelector("#sign-in-btn").addEventListener("click", () => {
      this.el.nativeElement.querySelector(".container").classList.remove("sign-up-mode");
    });  }

    signInClick() {
      if(this.userName == '' || this.password == '')
    {
      this._snackBar.open('Please enter email id and password.','Close', {
        duration: 5 * 1000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
      });
    }
    else
    {
      if(this.userName == 'admin' && this.password == 'admin') {
        localStorage.setItem('currentUser', 'new user')
        this.router.navigate(['/dashboard']);

      } else {
        this._snackBar.open('Please enter valid email id and password.','Close', {
          duration: 5 * 1000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });

      }

    }
          // alert(this.userName + " " + this.password)

  }

  signUpClick(){

  }
}

export interface CardData {
  imageId: string;
  state: 'default' | 'flipped' | 'matched';
}
