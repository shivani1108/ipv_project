import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalysisStructureComponent } from './analysis-structure/analysis-structure.component';
import { CreateAnalysisComponent } from './create-analysis/create-analysis.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GenerateReportComponent } from './generate-report/generate-report.component';
import { ImgProcessComponent } from './img-process/img-process.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AuthGuardService } from './_services/auth-guard.service';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'',component:SidebarComponent, canActivate: [AuthGuardService], children:[
    {path:'dashboard',component:DashboardComponent},
    {path:'imgProcess',component:ImgProcessComponent},
    {path:'createAnalysis',component:CreateAnalysisComponent},
    {path:'generateReport',component:GenerateReportComponent}
    // {path:'analysisStructure',component:AnalysisStructureComponent}

   
  ]},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
