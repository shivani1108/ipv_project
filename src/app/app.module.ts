import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';



import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { DashboardComponent } from './dashboard/dashboard.component';


import {InputTextModule} from 'primeng/inputtext';
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ButtonModule} from 'primeng/button';
// import {StyleClassModule} from 'primeng/api';
import {CheckboxModule} from 'primeng/checkbox';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {TableModule} from 'primeng/table';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import {SidebarModule} from 'primeng/sidebar';
import {DropdownModule} from 'primeng/dropdown';
import {StepsModule} from 'primeng/steps';
import {ToastModule} from 'primeng/toast';
import {CardModule} from 'primeng/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {GalleriaModule} from 'primeng/galleria';
import {FileUploadModule} from 'primeng/fileupload';
import {PickListModule} from 'primeng/picklist';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {ProgressBarModule} from 'primeng/progressbar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';





import { SidebarComponent } from './sidebar/sidebar.component';
import { ImgProcessComponent } from './img-process/img-process.component';
import { HttpClientModule } from '@angular/common/http';
import { AnalysisStructureComponent } from './analysis-structure/analysis-structure.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { CreateAnalysisComponent } from './create-analysis/create-analysis.component';
import { SizeRangeConfigurationComponent } from './size-range-configuration/size-range-configuration.component';
import { GenerateReportComponent } from './generate-report/generate-report.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    DashboardComponent,
    SidebarComponent,
    ImgProcessComponent,
    AnalysisStructureComponent,
    CreateAnalysisComponent,
    SizeRangeConfigurationComponent,
    GenerateReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    CheckboxModule,
    MatInputModule,
    ButtonModule,
    CheckboxModule,
    MatTabsModule,
    MatToolbarModule,
    TableModule,
    MatButtonModule,
    MatListModule,
    MatExpansionModule,
    MatIconModule,
    MatSidenavModule,
    MatCardModule,
    SidebarModule,
    DropdownModule,
    MatCheckboxModule,
    ToastModule,
    CardModule,
    InputTextModule,
    MatIconModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    GalleriaModule,
    FileUploadModule,
    PickListModule,
    MatSnackBarModule,
    HttpClientModule,
    ProgressBarModule,
    NgApexchartsModule,
    MatDialogModule,
    MatGridListModule

    
 ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
