import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SizeRangeConfigurationComponent } from './size-range-configuration.component';

describe('SizeRangeConfigurationComponent', () => {
  let component: SizeRangeConfigurationComponent;
  let fixture: ComponentFixture<SizeRangeConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SizeRangeConfigurationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SizeRangeConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
