import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-size-range-configuration',
  templateUrl: './size-range-configuration.component.html',
  styleUrls: ['./size-range-configuration.component.scss']
})
export class SizeRangeConfigurationComponent implements OnInit {

  constructor( @Inject(MAT_DIALOG_DATA) public data: any,
  public dialogRef: MatDialogRef<SizeRangeConfigurationComponent>,) { }

  details : any[] = []
  ngOnInit(): void {

    this.details = [
      {
        min : 0,
        max : 2.5
      },
      {
        min : 2.5,
        max : 5
      },
      {
        min : 5,
        max : 10
      },
     
    ]
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


