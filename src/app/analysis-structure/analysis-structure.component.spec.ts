import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisStructureComponent } from './analysis-structure.component';

describe('AnalysisStructureComponent', () => {
  let component: AnalysisStructureComponent;
  let fixture: ComponentFixture<AnalysisStructureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalysisStructureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
