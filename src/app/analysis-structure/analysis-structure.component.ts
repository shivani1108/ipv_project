import { Component, OnInit, ViewChild } from '@angular/core';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexStroke,
  ApexMarkers,
  ApexYAxis,
  ApexGrid,
  ApexTitleSubtitle,
  ApexLegend,
  ApexTooltip,
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexFill,

} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
  tooltip: ApexTooltip;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  
  
};









@Component({
  selector: 'app-analysis-structure',
  templateUrl: './analysis-structure.component.html',
  styleUrls: ['./analysis-structure.component.scss']
})
export class AnalysisStructureComponent implements OnInit {

  @ViewChild('chart') chart: ChartComponent | any
  //#1 - distribution
  public chartOptions: Partial<ChartOptions> | any

  //#2 - size left top
  public particleSizeChartOptions: Partial<ChartOptions> | any

  //#4 - type right top
  public particleTypeChartOptions: Partial<ChartOptions> | any

  //#5 - distribution - image
  public distributionImageChartOptions : Partial<ChartOptions> | any

  particleTypes : any[] = []
  selectedTypeId : any 
  particleDetails : any[] = []
  showData : boolean = false
  particleDistributionData: any[] = []
  particleDistributionSeries : any[] = []
  particleDistributionCategories : any[] = []

  particleImageDistributionData : any[] = []
  particleImageSeries : any[] = []
  particleImageCategories : any[] = []
  
  particleNosData : any[] = []
  particleNosSeries : any[] = []
  particleNosCategories : any[] = []

  particleClassificationData : any[] = []
  particleClassificationSeries : any[] = []
  particleClassificationCategories :any[] = []

  particleDistDetails : any[] = []
  sizeRange : any[] = []


  
  constructor() { 
 
  }

  // public generateData(baseval : any, count : any, yrange : any) {
  //   var i = 0;
  //   var series = [];
  //   while (i < count) {
  //     var x = Math.floor(Math.random() * (750 - 1 + 1)) + 1;
  //     var y =
  //       Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
  //     var z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;

  //     series.push([x, y, z]);
  //     baseval += 86400000;
  //     i++;
  //   }
  //   return series;
  // }
  
  

  ngOnInit(): void {
   this.getParticleDetails()
   this.particleDetails = [
    {
      type : 'Agglomeration',
      min : 0.50,
      max : 8.83,
      avg : 2.47,
      total : 88
    },
    {
      type : 'Particle',
      min : 0.48,
      max : 7.29,
      avg : 1.52,
      total : 105
    },
   
  ]
   this.particleDistDetails = [
    {
      type : 'Agglomeration',
      D10 : 0.57,
      D50 : 1.91,
      D90: 6.10
    },
    {
      type : 'Particle',
      D10 : 0.53,
      D50 :0.78,
      D90: 4.27
    },
   
  ]

  this.sizeRange = [
    {
      range : '0.00-2.50',
      count : 139,
      countP : 72.02,
      
    },
    {
      range : '2.50-5.00',
      count : 35,
      countP : 18.13,
      
    },
    {
      range : '5.00-10.00',
      count : 19,
      countP : 9.84,
      
    },
    {
      range : '10.00-20.00',
      count : 0,
      countP : 0,
      
    },
    {
      range : '20.00-30.00',
      count : 0,
      countP : 0,
      
    },
    {
      range : '30.00-50.00',
      count : 0,
      countP : 0,
      
    },
    {
      range : 'Total Particles',
      count : 193,
      countP : 100.00,
      
    },
   
  ]
    //#particle distribution
   this.particleDistributionData = [
    {
        "series": [
            {
            name: "p1", //particle-1 name
            data: [28, 29, 33, 36, 32] //particle size wrt distribution values(categories)
          },
          {
            name: "p2", //particle-1 name
            data: [12, 11, 14, 18, 17] //particle size wrt distribution values(categories)
          }        
        ],

        "categories": [
            "D-1",
            "D-21",
            "D-41",
            "D-61",
            "D-81"
     
        ]
    }
  ] 

   this.particleDistributionSeries =  JSON.parse(JSON.stringify(this.particleDistributionData[0]["series"]));
   this.particleDistributionCategories = JSON.parse(JSON.stringify(this.particleDistributionData[0]["categories"]));
   this.particleDistribution()

    //#particle - distribution - image
    this.particleImageDistributionData = [
      {
        "series": [
          {
            name: "D10",  
            data: [0,10,30,76]
          },
          {
            name: "D50",
            data: [0,41,20,55]
          },
          {
            name: "D90",
            data: [0,32,20,5]
          }
        ],
          "categories": [
            "Img2(98)",
            "Img4(107)",
            "Img6(180)",
            "Img8(193)",
       
          ]
      }
    ] 
    this.particleImageSeries = JSON.parse(JSON.stringify(this.particleImageDistributionData[0]["series"]));
    this.particleImageCategories = JSON.parse(JSON.stringify(this.particleImageDistributionData[0]["categories"]));
    this.particleImageDistribution()

      //#particle - classification
      this.particleClassificationData = [
        {
          "series": [46,54], //percentage of each particle type
            "categories": [ //particle types identified in current analysis
              'Agglomeration',
              'Particle'
         
            ]
        }
      ] 
      this.particleClassificationSeries = JSON.parse(JSON.stringify(this.particleClassificationData[0]["series"]));
      this.particleClassificationCategories = JSON.parse(JSON.stringify(this.particleClassificationData[0]["categories"]));
      this.particleClassification()

     //#particle nos
      this.particleNosData = [
        {
          "series": [
            {
              name: "P1", //particle name
              data: [31, 40, 28, 51, 42, 109, 100] // particle count wrt size(categories)
            },
            {
              name: "P2",
              data: [11, 32, 45, 32, 34, 52, 41]
            },
          ],
            "categories": [
              "0",
              "2",
              "4",
              "6",
              "8",
              "10",
              "12"
         
            ]
        }
      ] 
      this.particleNosSeries = JSON.parse(JSON.stringify(this.particleNosData[0]["series"]));
      this.particleNosCategories = JSON.parse(JSON.stringify(this.particleNosData[0]["categories"]));
      this.particleNos()
  }

   
  getParticleDataByType(value : any) {

  }

  getParticleDetails(){
    if(this.particleDetails.length <= 0){
      this.showData = false
    } else {
      this.showData = true

    }
  }

      //#particle distribution
  particleDistribution(){
      this.chartOptions = {
        series: this.particleDistributionSeries,
        chart: {
          height: 350,
          type: "line",
          dropShadow: {
            enabled: true,
            color: "#000",
            top: 18,
            left: 7,
            blur: 10,
            opacity: 0.2
          },
          toolbar: {
            show: false
          }
        },
        colors: ["#F9CE26", "#545454"],
        dataLabels: {
          enabled: true
        },
        stroke: {
          curve: "smooth"
        },
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: "bottom"
              }
            }
          }
        ],
        // title: {
        //   text: "Average High & Low Temperature",
        //   align: "left"
        // },
        grid: {
          borderColor: "#e7e7e7",
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        markers: {
          size: 1
        },
        xaxis: {
          categories: this.particleDistributionCategories,
          title: {
            text: "Distribution Values"
          }
        },
        yaxis: {
          title: {
            text: "Size (μ)"
          },
          min: 5,
          max: 40
        },
        legend: {
          position: "top",
          horizontalAlign: "right",
          floating: true,
          offsetY: -25,
          offsetX: -5
        }
      };
  }

     //#particle - distribution - image
  particleImageDistribution(){
     this.distributionImageChartOptions = {
      series: this.particleImageSeries,
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: false
        }
      },
      colors: ["#F9CE26", "#545454","#F44336"],
      dataLabels: {
        enabled: true
      },
      stroke: {
        curve: "straight"
      },
     
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      xaxis: {
        categories: this.particleImageCategories,
        title: {
          text: "Images"
        }
      }
    };
  }

    //#particle - classification
  particleClassification(){
        this.particleTypeChartOptions = {
          series: this.particleClassificationSeries,
          chart: {
            type: "polarArea",
            height: 380
          },
          labels : this.particleClassificationCategories,
          // stroke: {
          //   colors: ["#fff"]
          // },
          fill: {
            opacity: 0.8
          },
          
          colors: ["#F9CE26", "#545454"],
          responsive: [
            {
              breakpoint: 480,
              options: {
                chart: {
                  width: 200
                },
                legend: {
                  position: "bottom"
                }
              }
            }
          ]
        };
  }

    //#particle nos
  particleNos(){
  this.particleSizeChartOptions = {
    series: this.particleNosSeries,
    colors: ["#F9CE26", "#545454"],
    chart: {
      height: 310,
      type: "area"
    },
    dataLabels: {
      enabled: true
    },
    stroke: {
      curve: "smooth"
    },
    
    xaxis: {
      categories: this.particleNosCategories,
      title: {
        text: "Particle Size"
      }
    },
    yaxis: {
      title: {
        text: "Particle Count (%)"
      },
      min: 0,
      max: 100
    },
    tooltip: {
      x: {
        format: "dd/MM/yy HH:mm"
      }
    },
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200
          },
          legend: {
            position: "bottom"
          }
        }
      }
    ]
  };
  }

}
