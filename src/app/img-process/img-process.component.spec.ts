import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgProcessComponent } from './img-process.component';

describe('ImgProcessComponent', () => {
  let component: ImgProcessComponent;
  let fixture: ComponentFixture<ImgProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImgProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
