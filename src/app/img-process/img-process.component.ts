import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ProcessService } from '../_services/process.service';
import { UploadImageService } from '../_services/upload-image.service';

@Component({
  selector: 'app-img-process',
  templateUrl: './img-process.component.html',
  styleUrls: ['./img-process.component.scss'],
  providers: [MessageService,DatePipe]

})
export class ImgProcessComponent implements OnInit {

  imageSrc :any = ''
  showSpinner = 'N'
  showText ='Y'
  uploadedImage : any[] = []
  processedImage : any = ''
  uploaded : boolean = false
  processed : boolean = false
  inProcess : boolean = false
  details : any[] = []
  showAn : boolean = false
  showDetails : boolean = false
  showGrid : boolean = false
  constructor( private messageService: MessageService,
    private datepipe : DatePipe,
    private uploadImageService : UploadImageService,
    private processService : ProcessService,
    private el: ElementRef) { }

  ngOnInit(): void {

    // this.el.nativeElement.querySelector("#imgUp").addEventListener("click", () => {
     
    // });
    this.details = [
      {
        parameter : 'Total Images',
        value : 1
      },
      {
        parameter : 'Agglomeration',
        value : 95
      },
      {
        parameter : 'Total Count',
        value : 95
      },
      {
        parameter : 'D10',
        value : 0.55
      },
      {
        parameter : 'D50',
        value : 0.92
      },
      {
        parameter : 'D90',
        value : 5.25
      }
    ]

  }

  readURL(event:any){
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;

      reader.readAsDataURL(file);
      for (let file of event.target.files) 
      {
        
        var fileName = '';
        // filename should be unique for all records
        // FORMAT : yyyyMMddHHmmss_brandId_vendorId_filename(with extension)
        fileName = this.datepipe.transform( new Date(), 'yyyyMMddHHmmss') + '_'  + file.name
      
        this.uploadImageService.uploadImage(file,fileName);
        setTimeout(() => {
          this.checkStatus();
        }, 2500);
       
       
  
      } 
  
  }
  }

  checkStatus()
  {
    var res = this.uploadImageService.success()
    if(!res['location'].Location)
    {
      setTimeout(() => {
        this.checkStatus();
      }, 2000);      
    }
    else
    {   
      this.uploaded = res.imageInProcess
      this.uploadedImage.push(res['location'].Location)

    }
    if(this.uploadedImage.length > 1) {
      this.showGrid = true
    }

  }

  processClick(index : any) {
    this.inProcess = true
    this.processService.processImage(this.uploadedImage[index]).then((data : any) => {
      if(data){ 
        this.inProcess = false
        this.processedImage = data['data'].path
        this.messageService.add({severity:'success', summary:'Success', detail:'Image processing.'});
        setTimeout(() => {
        }, 1800); 
        this.showAn = true
      }
      else {
        this.messageService.add({severity:'failure', summary:'Failure', detail:'Error while processing image.'});
        setTimeout(() => {
        }, 1800); 
      }
    })

  }

  showAnalysis(){
    this.showDetails = true
  }

}
