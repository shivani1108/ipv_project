import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { serverUrl } from '../serverUrl';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient : HttpClient,
    private router: Router) { }

    authenticateUser(usrName : string , usrPassword : string)
  {
    // var loginUrl = serverUrl +  'authenticateUserV1?usrName=' + 
    // usrName + '&usrPassword=' + usrPassword;   

    // return new Promise(resolve => 
    // {
    //   this.httpClient.get(loginUrl).toPromise().then(res => {
    //     resolve(res);      
    //   }).catch((err : any)=> {
    //     resolve(err);
    //   });     
    // });
   
  }

    logout() {
      localStorage.removeItem('currentUser');
    }

    isAuthenticateUser(): boolean {
      if (localStorage.getItem('currentUser')) {
        return true;
      }
     
      else {
        this.router.navigate(['/']);
        return false;
      }
    }
}

