import { TestBed } from '@angular/core/testing';

import { AnalysisStructureService } from './analysis-structure.service';

describe('AnalysisStructureService', () => {
  let service: AnalysisStructureService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnalysisStructureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
