import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { serverUrl } from '../serverUrl';

@Injectable({
  providedIn: 'root'
})
export class ProcessService {

  constructor(private httpClient : HttpClient) { }

  processImage(path : any) {

    var url = serverUrl + 'findparticles?path=' + path
    
     return new Promise(resolve => 
    {
      this.httpClient.get(url).toPromise().then(res => {
        resolve(res);      
      }).catch((err : any)=> {
        resolve(err);
      });     
    });
  }

}
