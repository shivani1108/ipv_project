import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { serverUrl } from '../serverUrl';

@Injectable({
  providedIn: 'root'
})
export class CreateAnalysisService {

  constructor(private httpClient : HttpClient) { }

  createAnalysis(analysisId : any,analysisName : any,arNo : any,batchNo : any,method : any,imageModel : any,particleModel : any) {

    var url = serverUrl + 'createanalysis?analysisId=' + analysisId + '&analysisName=' + analysisName  + '&arNo=' + arNo + '&batchNo=' + batchNo 
              '&method=' + method + '&imageModel=' + imageModel + '&particleModel=' + particleModel 
    
    
     return new Promise(resolve => 
    {
      this.httpClient.get(url).toPromise().then(res => {
        resolve(res);      
      }).catch((err : any)=> {
        resolve(err);
      });     
    });
  }

  processImage(path : any) {

    var url = serverUrl + 'findparticles?path=' + path
    
     return new Promise(resolve => 
    {
      this.httpClient.get(url).toPromise().then(res => {
        resolve(res);      
      }).catch((err : any)=> {
        resolve(err);
      });     
    });
  }
}
