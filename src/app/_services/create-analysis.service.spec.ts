import { TestBed } from '@angular/core/testing';

import { CreateAnalysisService } from './create-analysis.service';

describe('CreateAnalysisService', () => {
  let service: CreateAnalysisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateAnalysisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
