import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as S3 from 'aws-sdk/clients/s3';

@Injectable({
  providedIn: 'root'
})
export class UploadImageService {
  uploadedImageLocation: any;
  inProcess : boolean = false

  constructor(private _snackBar : MatSnackBar) { }

  uploadImage(file : any , fileName : any) {
    const contentType = file.type;
    const bucket = new S3(
          {
              accessKeyId: 'AKIAZ3GSYD4DH2Y6WWXU',
              secretAccessKey: '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',
              region: 'ap-south-1'
          }
      );
      const params = {
          Bucket: 'ipv-find-particles',
          Key: fileName,
          Body: file,
          ACL: 'public-read',
          ContentType: contentType
      };
      bucket.upload(params,  (err : any, data : any) => {
          if (err) {
              console.log('There was an error uploading your file: ', err);
              // return false;
          } else {
            console.log('Successfully uploaded file.', data);
            this.uploadedImageLocation = data;
            this.inProcess = true
            this._snackBar.open('Successfully uploaded file.','Close', {
              duration: 3 * 1000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });          
            this.success()
            // return true;
          }
         
      });
  }

   success()
  {
    return {
      location : this.uploadedImageLocation,
      imageInProcess : true
    }
  }

  failure()
  {
    return 'FAILURE'
  }
}
