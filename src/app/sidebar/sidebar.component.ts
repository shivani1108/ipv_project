import { ChangeDetectorRef, Component, OnInit, ViewRef } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [MatSidenav]

})
export class SidebarComponent implements OnInit {

  screenWidth : any
  isExpanded : boolean = false;
  sidenavContent : any[] = [];
  usrName : any = ''

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  constructor(private breakpointObserver: BreakpointObserver,
    private router : Router,
    private sidenav: MatSidenav,
    private cdRef: ChangeDetectorRef) {

      this.screenWidth = window.innerWidth;
      window.onresize = () => {
        //   // set screenWidth on screen size change
          this.screenWidth = window.innerWidth;
          // this.cdr.detectChanges();
          setTimeout(() => {
            if (this.cdRef !== null && this.cdRef !== undefined &&
              !(this.cdRef as ViewRef).destroyed) {
              this.cdRef.detectChanges();
            }
          }, 250);
        }
     }

  ngOnInit(): void {

    this.sidenavContent = [
      {menuClickLabel : 'dashboard' , label : 'Dashboard' , icon : 'dashboard'},
      {menuClickLabel : 'createAnalysis' , label : 'Create Analysis' , icon : 'app_registration'},
      {menuClickLabel : 'logout', label : 'Logout' , icon : 'logout'}
      // {menuClickLabel : 'imgProcess' , label : 'Particle Analysis' , icon : 'app_registration'},
      // {menuClickLabel : 'analysisStruct', label : 'Analysis Structure', icon : 'insert_chart_outlined'}
    
      
    ];

  
  }

  selectedDashboardLink : string = '';
  onMenuClick(menuItem : any)
  {
    this.selectedDashboardLink = menuItem;
   if(menuItem == 'dashboard')
    {
      this.router.navigate(['/dashboard'])

    }
    // else if(menuItem == 'imgProcess')
    // {
    //   this.router.navigate(['/imgProcess'])

    // } 
    // else if(menuItem == 'analysisStruct')
    // {
    //   this.router.navigate(['/analysisStructure'])

    // } 
    else if(menuItem == 'createAnalysis')
    {
      this.router.navigate(['/createAnalysis'])

    } 
    else if(menuItem == 'logout')
    {
      if(this.usrName == 'lomadmin')
      {
        this.router.navigate(['/lomLanding'])
        localStorage.clear();
      }
      else
      {
        this.router.navigate(['/'])
        localStorage.clear();
      }    

    } 
   
   

    
   
    // end
    this.cdRef.detectChanges(); 
    // sidenav.close();
  }

  onLogoutClick()
  {
    if(this.usrName == 'lomadmin')
    {
      this.router.navigate(['/lomLanding'])
      localStorage.clear();
    }
    else
    {
      this.router.navigate(['/'])
      localStorage.clear();
    }    
  }

}
