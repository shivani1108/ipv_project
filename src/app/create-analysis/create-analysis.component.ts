import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, NavigationEnd } from '@angular/router';
import { MessageService } from 'primeng/api';
import { SizeRangeConfigurationComponent } from '../size-range-configuration/size-range-configuration.component';
import { CreateAnalysisService } from '../_services/create-analysis.service';

@Component({
  selector: 'app-create-analysis',
  templateUrl: './create-analysis.component.html',
  styleUrls: ['./create-analysis.component.scss'],
  providers: [MessageService]

})
export class CreateAnalysisComponent implements OnInit {

  constructor( public dialog: MatDialog,
    private createAnalysisService : CreateAnalysisService,
    private messageService: MessageService,
    private _snackBar : MatSnackBar,
    private router : Router
    ) { }

  analysisId : any = ''
  analysisName : any = ''
  arNo : any = ''
  batchNo : any = ''
  method : any = ''
  imageModel : any = ''
  particleModel : any = ''
  analysis : any[] = []
  imgModel : any[] = []
  pmModel : any[] = []
  isDisabled : boolean = true 
  showNext : boolean = false

  ngOnInit(): void {
    // this.analysis = [
    //   {
    //     id: 1,
    //     name: 'Analyis-1'
    //   },
    //   {
    //     id: 2,
    //     name: 'Analyis-2'
    //   },
    //   {
    //     id: 3,
    //     name: 'Analyis-3'
    //   }
    // ]
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
  });
    this.analysis = [
      {name: 'Analyis-1'},
      {name: 'Analyis-2'},
      {name: 'Analyis-3'}
     
  ];

    this.imgModel = [
      {
        id : 1,
        name : 'IM-1'
      },
      {
        id : 2,
        name : 'IM-2'
      },
      {
        id : 3,
        name : 'IM-3'
      }
    ]

    this.pmModel = [
      {
        id : 1,
        name : 'PM-1'
      },
      {
        id : 2,
        name : 'PM-2'
      },
      {
        id : 3,
        name : 'PM-3'
      }
    ]

    
  }

  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;

  }

  createAnalysis(){
    this.showNext = true

    this.createAnalysisService.createAnalysis(this.analysisId,this.analysisName,this.arNo,this.batchNo,this.method,this.imageModel,this.particleModel).then((data : any) => {
      if(JSON.parse(JSON.stringify(data.status)) == 'SUCCESS')
      {
        this._snackBar.open('Analysis created successfully..','Close', {
          duration: 3 * 1000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });  
        // this.messageService.add({severity:'success', summary:'Success', detail:'Analysis created successfully.'});
        // setTimeout(() => {
        // }, 1800);
    
      }
      else
      {
        this._snackBar.open('Error while creating analysis.','Close', {
          duration: 3 * 1000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });  
          this.messageService.add({severity:'error', summary:'Failed', detail:'Error while creating analysis.'});

       
      }
    })
  }

  prevStep() {
    this.step--;
  }

  configSizeRange(){
    const dialogRef = this.dialog.open(SizeRangeConfigurationComponent,{
      width: '550px',
      disableClose: true,
      data : {
        isEditMode : false,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
      if(result == 'Success')
      {
      
      }
    });
  }

  generateReport(){
    this.router.navigate(['/generateReport'])
  }
 

}
